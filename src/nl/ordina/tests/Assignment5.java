package nl.ordina.tests;

import nl.ordina.webdriver.BrowserHelper;
import org.openqa.selenium.WebDriver;

public class Assignment5 {

    private WebDriver browser;

    public Assignment5() {
        this.browser = BrowserHelper.getChromeBrowser();
    }

    public void goToUrl() {
        browser.get("http://www.google.nl");
    }
}
